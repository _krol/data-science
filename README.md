# README #
Miscellaneous scripts related to Data Science studies.  

### Work Sources ###

* Berkeley Stats 133
* Doing Data Science; O'Neill, Schutt; O'Reilly Media
* Think Stats; Downey; O'Reilly Media
* Think Bayes; Downey; O'Reilly Media