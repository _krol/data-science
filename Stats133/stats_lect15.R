coinflip = function(p=.5) {
  sample(c(1, 0), size = 1, prob = c(p, 1-p))
}

# how many flips does it take to get (total) heads
negbinom = function(p = 0.5, total = 10, maxflips = 10^6){
  flips = 0
  heads = 0
  while(heads < total & flips < maxflips){
    heads = heads + coinflip(p)
    flips = flips + 1
  }
  
  if(heads == total) {
    return(flips)
  } else {
    stop("Reached max number of flips")
  }
}

# right a function coinflips(n,p) that will return a vector of
# n values of 0 or 1
# you can do this using a for loop, an apply function, or neither
coinflips = function(n,p=.5) {
  sample(c(1,0),size=n,prob=c(p,1-p),replace=T)
}

system.time(coinflips(10000))
