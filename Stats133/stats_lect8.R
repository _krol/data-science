load(url("http://www.stat.berkeley.edu/users/nolan/data/KaiserBabies.rda"))

hist(infants$bwt, freq=FALSE, xlab="birth weight (oz)", main="Male Babies, Oakland Kaiser 1960s")
plot(density(infants$bwt,bw=1))
barplot(table(infants$parity))
