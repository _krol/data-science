# DDS_Ex2_RDDS.R
# Excersize 2 - RealDirect Data Strategy
# Page 48

# - From the Text
## - Me

# You have been hired as chief data scientist at realdirect.com, and report
# directly to the CEO. The company (hypotetically) does not yet have its data
# plan in place. It's looking to you to come up with a data strategy. Here are 
# a couple ways you could begin to approach the problem:

# 1. Explore its existing website. Understand the existing business model, and 
#    think about how analysis of RealDirect user-behavior data could be used to
#    inform decision-making and product development. 

## Analysizing user behavior helps us understand what is important to the user. 
## Users are using the product to try and do something (sell real-estate). 
## Shifting product development to most used areas may mean the users have a 
## better experience using the product. Alternatively, if users aren't using a 
## feature, better develop that feature or get rid of it (stop wasting 
## resources). 

# a. What would you advise engineers to log?
## 1. landing page - visitors, clicks for Sell Your home, clicks for Find Your 
##    Home, About User interaction
## 2. How many users are stopped by Registration Wall
##   - Should we offer some kind of 'how it works' so people know why they're 
##     signing up?
# a. What would Ideal DataSets look like
## This is tough. I'm used to log files. So, my initial thought is timestamps,
## Clicks, UIDs (to track users), Registered/logged in.. that sort of thing. 
## However, I'm still trying to understand what we're trying to learn. If we're
## trying to understand what it takes for users to sign up, then we would log 
## different things than if we're trying to improve the core product.
## Also, trying to figure out a price point might be a good idea, maybe the $395
## is too low, or too high. 

# b. How would data be used for reporting and monitoring product usage?
## We're trying to report product usage. So, identifying most used areas, how
## many people are stopped by Registration wall, Who comes to our site (Buyers 
## vs Realitors), and how the search functionality is used would make sense to
## generate some numbers around.

# c. How would Data be built back into the website?
## We'd try to identify some key areas where users get halted and try to improve
## them. 

# 2. Background Research: Go https://github.com/oreillymedia/doing_data_science
#    and get Rolling Sales Update. Use Any or All data sets. 

# a. Load in and Clean Data
# b. EDA the clean Data

# 3. Summarize data in report aimed at the CEO
## What do CEOs consider important?

# 4. As a DS, who would you talk to for getting the data you need?
## Realtors - How do they interface with people to find homes? Do they look for
## certain keywords, or by amenities? Do they present people a list of a few 
## homes to consider? How vague are their clients? 

## The following is my answer to 2. Note that I will probably have to use a 
## great deal of the code from the book. Credit goes to Benjamin Reddy. His 
## comments with be with a single hash, mine will continue to be in double. 

## Get the data
install.packages('gdata')  ## allows us to import .xls
require('gdata')
MHData = read.xls("/Users/cynikre/Dropbox/devs/DataSci/DDS/DATA/rollingsales_manhattan.xls",
                  pattern = 'BOROUGH')

## Lets take a look at the data to see what we're working with. 
## Note, you can visually explore data from RStudio and excell.
## Use View(DATA)

head(MHData)
## Manhattan has Boroughs and neighbourhoods. A neighbourhood is in a borough.
summary(MHData)
## This is ugly in a small console

## We want to start converting Factors into numerics (because numerics are 
## more useful). 

## SALE PRICE
head(MHData$SALE.PRICE) ## some have '$', some have commas
## We're using a expression to just get the digits
MHData$SALE.PRICE = as.numeric(gsub("[^[:digit:]]","",MHData$SALE.PRICE))
## Check it
head(MHData$SALE.PRICE)
length(MHData$SALE.PRICE) ## 27395
length(MHData$SALE.PRICE[MHData$SALE.PRICE == 0]) ## 7593

## GROSS SQUARE FEET
MHData$GROSS.SQUARE.FEET = as.numeric(gsub("[^[:digit:]]","",MHData$GROSS.SQUARE.FEET))

## LAND SQURE FEET
MHData$LAND.SQUARE.FEET = as.numeric(gsub("[^[:digit:]]","",MHData$LAND.SQUARE.FEET))

## Year Built
MHData$YEAR.BUILT = as.numeric(as.character(MHData$YEAR.BUILT))

## Convert to Dates
MHData$SALE.DATE = as.Date(MHData$SALE.DATE)

## Now our data is cleaner. We have some empty values, but thats ok

attach(MHData)

hist(SALE.PRICE) ## not so useful
hist(SALE.PRICE[SALE.PRICE.N > 0]) ## looks like the same chart
hist(GROSS.SQUARE.FEET[SALE.PRICE > 0])
max(SALE.PRICE)

## Ok, so whats happening is that we have a huge number of values betwen 0
## and $1,000,000. We also have values up to 1.3 billion. 
plot(SALE.PRICE)
plot(x=GROSS.SQUARE.FEET, y=SALE.PRICE)

## Lets get rid of stupid values
detach(MHData)
MHSales = MHData[MHData$SALE.PRICE != 0,] ## stupid comma

plot(MHSales$GROSS.SQUARE.FEET, MHSales$SALE.PRICE)
plot(log(MHSales$GROSS.SQUARE.FEET), log(MHSales$SALE.PRICE)) ## why log?


