""" Think Stats Chapter 1 """

import survey

table = survey.Pregnancies()
table.ReadRecords('DATA/')

print 'number of pregnancies', len(table.records)

""" Write a loop that  counts the number of live births """

i=0
for record in table.records:
    if getattr(record, 'outcome') == 1:
        i = i + 1

print 'Live births: ', i 

livebabies = []
for record in table.records:
    if getattr(record, 'outcome') == 1:
        livebabies.append(record)
        
firstbabies = []
for baby in livebabies:
    if getattr(baby, 'birthord') == 1:
        firstbabies.append(baby)

print 'firstbabies: ', len(firstbabies)
print 'otherbabies: ', len(livebabies) - len(firstbabies)

firstGestation = 0
for baby in firstbabies:
    firstGestation = firstGestation + getattr(baby, 'prglength')

print 'avg gestation first babies: ', firstGestation / len(firstbabies)

otherGestation = 0
for baby1 in livebabies:
    if getattr(baby1, 'birthord') != 1:
        otherGestation = otherGestation + getattr(baby1, 'prglength')

print 'avg gestation first babies: ', otherGestation / (len(livebabies) - len(firstbabies))